﻿(function () {
    angular.module('zfTabsApp')
        .directive('zfTab', zfTab);

    zfTab.$inject = [];

    function zfTab() {
        const directive = {
            scope: {
              tabTitle:'<'  
            },
            restrict: 'E',
            transclude: true,
            template:
                `<div id="{{divId}}" ng-show="parentCtrl.activeTab===divId">
                    <ng-transclude></ng-transclude>
                </div>`,
            require:'^zfTabs',
            link: linkFn
        }

        return directive;

        //////
        function linkFn(scope, element, attr, controller) {
            scope.divId = controller.addLabel(scope.tabTitle);
            scope.parentCtrl = controller;
        }
    }
})();