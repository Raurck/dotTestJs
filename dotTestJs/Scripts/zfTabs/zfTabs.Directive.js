﻿(function () {
    angular.module('zfTabsApp')
        .directive('zfTabs', zfTabs);

    zfTabs.$inject = [];

    function zfTabs() {
        const directive = {
            restrict: 'E',
            transclude: true,
            template:
                `<div class="zf-tabs">
                    <ul>
                     <li ng-repeat="label in zfVm.labels"  ng-class ="{'active':zfVm.activeTab==='{{zfVm.tabPrefix}}_{{$index}}'}">
                                <div data-tabid="{{zfVm.tabPrefix}}_{{$index}}" ng-click="zfVm.handleClick($event)">{{label}}</div>
                            </li>
                    </ul>
                    <div>
                        <ng-transclude></ng-transclude>
                    </div>
              </div>`,
            controller: zfTabsCtrl,
            controllerAs: 'zfVm',
            bindToController: {
                tabPrefix: '@',
                activeTabIdx: '<'
            }
        }

        return directive;

    }

    zfTabsCtrl.$inject = [];
    function zfTabsCtrl() {
        const zfVm = this;
        zfVm.addLabel = addLabel;
        zfVm.handleClick = handleClick;

        init();
        ///////
        function init() {
            zfVm.labels = [];
            zfVm.activeTab = null;
        }



        ///add new tab into list
        function addLabel(newLabel) {
            if (zfVm.activeTab === null && zfVm.activeTabIdx <= zfVm.labels.length) {
                zfVm.activeTab = `${zfVm.tabPrefix}_${zfVm.activeTabIdx}`;
            }
            return `${zfVm.tabPrefix}_${zfVm.labels.push(newLabel) - 1}`;
        }

        ///activate tab
        function handleClick($event) {
            $event.preventDefault();
            $event.stopImmediatePropagation = true;

            const targetDiv = $event.target.attributes.getNamedItem('data-tabid').nodeValue;
            zfVm.activeTab = targetDiv;
        }
    }
})();