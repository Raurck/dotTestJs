﻿/// <binding ProjectOpened='Watch - Development' /> 

'use strict';

var WebpackNotifierPlugin = require('webpack-notifier');

module.exports = {
    entry: {
        zfDotApp: ['./App/app.js',
                    './App/app.config.route.js',
                    './App/app.config.js',
                    './App/Common/section.Factory.js',
                    './App/Common/section.Service.js',
                    './App/Sectioncards/sectionCardList.Component.js',
                    './App/Sectioncards/sectionCard.Component.js',
                    './App/Tabs/tabbedView.Component.js',
                    './Content/app.styl'],
        zfTabApp: [
                    './Scripts/zfTabs/zfTabsApp.js',
                    './Scripts/zfTabs/zfTabs.Directive.js',
                    './Scripts/zfTabs/zfTab.Directive.js',
                    './Scripts/zfTabs/zftabs.styl']
    },

    output: {
        filename: './Scripts/[name].js'
    },
    devServer: {
        contentBase: '.',
        host: 'localhost',
        port: 58740
    },
    module: {
        loaders: [
            {
                test: /\.js?$/,
                loader: 'babel-loader',
                query: {
                    presets: ['es2015']
                }
            },
            { test: /\.styl$/, loader: 'style-loader!css-loader!stylus-loader' }
        ]
    },

    plugins: [
        new WebpackNotifierPlugin()
    ]
};