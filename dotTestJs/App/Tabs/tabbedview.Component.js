﻿(function () {
    angular.module('zfDotTestJs')
        .component('zfTabbedView',
            {
                bindings: {
                },
                templateUrl: 'App/Tabs/tabbedView.html',
                controller: tabbedView,
                controllerAs: 'tvVm'
            });

    tabbedView.$inject = ['$interval', '$state', '$timeout', 'sectionService'];
    function tabbedView($interval, $state, $timeout, sectionService) {
        const tvVm = this;
        tvVm.$onInit = onInit;

        //////
        function onInit() {
            tvVm.sections = sectionService.sectionsList.filter(el => el.selected === true ? true : false);

            ///if no data activate countdown and redirect
            if (tvVm.sections.length === 0) {
                tvVm.countDown = 3;
                $interval(() => tvVm.countDown--, 1000, 3);
                $timeout(() => $state.go('default'), 3000);
            }
        }

    }
})();