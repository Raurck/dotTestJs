﻿(function () {
    angular.module('zfDotTestJs')
        .config(routeConfig);

    ///////
    ///configurate routes of application
    routeConfig.$inject = ['$stateProvider', '$urlRouterProvider'];
    function routeConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('root',
                {
                    abstract: true,
                    template: '<div class="root" ui-view=""></div>',
                    resolve: {
                        getSettingsValues: getSettingsValues
                    }
                })
            .state('default',
                {
                    parent: 'root',
                    url: '/',
                    template: '<zf-section-card-list></zf-section-card-list>'
                })
            .state('tabview',
                {
                    parent: 'root',
                    url: '/tabview',
                    template: '<zf-tabbed-view></zf-tabbed-view>'
                });

        $urlRouterProvider.otherwise('/');
    }


    //let us get all application startup data
    //+simulte time gap
    getSettingsValues.$inject = ['$q', '$log', 'sectionService'];

    function getSettingsValues($q, $log, sectionService) {
        return $q.all([sectionService.getSections(), $q(resolve =>setTimeout(() =>resolve(true), 2000))]);
    }
})();