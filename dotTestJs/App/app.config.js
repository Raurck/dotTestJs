﻿(function() {
    angular.module('zfDotTestJs')
        .config(configSectionService);

    configSectionService.$inject = ['sectionServiceProvider'];
    function configSectionService(sectionService)
    {
        sectionService.setDataUrl('App/Common/section.json');
    }
})();