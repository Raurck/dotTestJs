﻿(function () {
    angular.module('zfDotTestJs')
        .provider('sectionService', sectionServiceProvider);

    function sectionServiceProvider() {
        const ssp = this;
        let dataUrl = 'App/Common/section.json';
        ssp.setDataUrl = setDataUrl;
        ssp.$get = secService;

        function setDataUrl(newDataUrl) {
            dataUrl = newDataUrl;
        }

        secService.$inject = ['$http', '$log', 'sectionFactory'];
        function secService($http, $log, sectionFactory) {
            return new sectionService($http, $log, sectionFactory, dataUrl);
        }

    }

    function sectionService($http, $log, sectionFactory,dataUrl) {
        const self = this;
        self.sectionsList = [];
        self.getSections = getSections;

        //////
        ///get it from dataUrl, transform, return promise
        function getSections() {
            return $http.get(dataUrl).then(sectionDTOtoObj, showFlashError);
        }

        ///transform recived DTOs to typed objects
        function sectionDTOtoObj(responce) {
            const sections = [];

            ///single element  processing
            function toDto(element) {
                const item = new sectionFactory.Section();
                item.assingnData(element);
                sections.push(item);
            }

            ///check what we have responce and responce data is array
            if (responce && Array.isArray(responce.data)) {
                responce.data.map(toDto);
            }
            
            ///clean service array
            self.sectionsList.splice(0, self.sectionsList.length);
            ///feel service array with new data
            Array.prototype.push.apply(self.sectionsList, sections);

            ///we can chain more if we need it
            return sections;
        }


        ///we can inform about some crashes
        function showFlashError(responce) {
            ///todo add inform in user visible form
            $log.debug(responce);
        }

    }
})();