﻿(function () {
    angular.module('zfDotTestJs')
        .factory('sectionFactory', sectionFactoryFn);

    sectionFactoryFn.$inject = [];

    function sectionFactoryFn() {

        const factory = {
            Section: section
        }
        return factory;


        ///////
        //assign scalar properties of DTObj to typedObj
        function assignSimpleData(typedObj, dtObj) {

            function assignKey(value, key) {
                const keyU = key.charAt(0).toUpperCase() + key.slice(1);
                if (typeof typedObj[key] != 'function' &&
                    !Array.isArray(typedObj[key]) &&
                    (dtObj[key] || dtObj[keyU])) {
                    typedObj[key] = dtObj[key] || dtObj[keyU];
                }
            }

            angular.forEach(typedObj, assignKey);
        }


        //section. we can have our functions and additional fields wich was not supplied by DTObj
        function section() {
            const sect = this;
            sect.label = '',
            sect.content = '';
            sect.selected = false;
            sect.someAwesomeFunction = someAwesomeFunction;
            sect.assingnData = assignData;

            function someAwesomeFunction() {
                ///we can do some tricky stuff here.... or not
            }

            function assignData(dtObj) {
                assignSimpleData(sect, dtObj);
                ///here we can assign complex data such as array.
            }
        }
    }

})();