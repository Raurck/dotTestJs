﻿(function() {
    angular.module('zfDotTestJs')
        .component('zfSectionCardList',
            {
                templateUrl: 'App/Sectioncards/sectionCardList.html',
                controller: sectionCardList,
                controllerAs:'scLstVm'
            });

    sectionCardList.$inject = ['sectionService'];
    function sectionCardList(sectionService) {
        const scLstVm = this;
        scLstVm.$onInit = onInit;

        //////
        function onInit() {
            scLstVm.sections = sectionService.sectionsList;
        }
    }
})();