﻿(function () {
    angular.module('zfDotTestJs')
        .component('zfSectionCard',
            {
                bindings: {
                    section: '<',
                    index: '<?'
                },
                templateUrl: 'App/Sectioncards/sectionCard.html',
                controller: sectionCard,
                controllerAs: 'scVm'
            });

    sectionCard.$inject = [];
    function sectionCard() {
        const scVm = this;
        scVm.$onInit = onInit;

        //////
        function onInit() {
            scVm.index = scVm.index || 0;
        }
    }
})();