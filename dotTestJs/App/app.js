﻿(function () {
    'use strict';

    angular.module('zfDotTestJs', [
        'ngAnimate',
        'ui.router',
        'zfTabsApp'
    ]);
})();
